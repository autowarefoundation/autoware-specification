# Autoware Specification

The Autoware Specification is a set of documents that define what Autoware is.
It defines many aspects of Autoware, including:

- what Autoware is capable of as a self-driving framework,
- what the architecture of an Autoware implementation must be,
- what modules make up an Autoware implementation and what their interfaces are, and
- what a hardware platform must provide to allow an Autoware implementation to execute correctly.

The above is not an exhaustive list.


# Specification process

To be done.

Participation in this process is open to all.
You do not need to be a member of the Autoware Foundation.
